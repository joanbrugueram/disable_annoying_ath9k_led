# Disable annoying Ath9K Wireless LED (Linux) #

## The problem

In my home Linux system, I have an annoying wireless device that is constantly blinking with a bright green LED, which makes sleeping difficult.

![Adapter (light)](adapter.jpg)
![Adapter (no light)](adapter2.jpg)

(Intense amounts of suffering were caused by this device)

## The solution (with udev)

Under `/etc/udev/rules.d/10-network.rules`, I added a rule that calls a script (in addition to the rule I already had to give the adapter a consistent name). `XX:XX:XX:XX:XX:XX` should be replaced with the adapter's MAC which you can find with `ip addr`

```
SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="XX:XX:XX:XX:XX:XX", NAME="wireless0", RUN+="/etc/udev/stop_annoying_led.sh"
```

Then I created a script under `/etc/udev/stop_annoying_led.sh` with the following content:

```bash
#!/bin/bash
for d in /sys/class/leds/ath9k_htc* ; do
    echo none > "$d/trigger"
    echo 0 > "$d/brightness"
done
```

Then I ran `chmod +x /etc/udev/stop_annoying_led.sh` (give executable permissions), `udevadm control --reload-rules && udevadm trigger` (reload udev rules), and it was fixed after unplugging and plugging.

## Comments and the meaning of life

I tried to add `options ath9k_htc blink=0` to `/etc/modprobe.d`, and this made the blinking stop, however, the LED remained on by default.

What I like about this solution is that it is simple (no dependencies) and doesn't require any hacky stuff like polling periodically.

Also, this solution should be pretty easy to modify for any other WiFi adapter whose LEDs can be configured by software.

Also, I'm probably missing the sleep that this LED blinking should have saved me by typing this.
